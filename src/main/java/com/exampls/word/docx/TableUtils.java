package com.exampls.word.docx;

import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

/**
 * @Author : LukeRen
 * @DateTime: 2021/11/26 10:02
 * @Description : 表格操作工具类
 * @Version : 1.0
 */
public class TableUtils {

    /***********一种替代的方式 取出单元格边框 start********************/
    //这种取出表格的方式,限制于poi-ooxml-3.10-FINAL 版本
    //在可知的poi-ooxml-4.1.2 提供的专有方法删除单元格边框

    private enum Border { LEFT, TOP, BOTTOM, RIGHT }

    /**
     * 移除上下左右边框
     *
     * @param cell
     */
    public static void removeBorders(XWPFTableCell cell){
        for (Border border : Border.values()) {
            setTableCellBorder(cell, border, STBorder.NIL);
        }
    }

    static void setTableCellBorder(XWPFTableCell cell, Border border, STBorder.Enum type) {
        CTTc tc = cell.getCTTc();
        CTTcPr tcPr = tc.getTcPr(); if (tcPr == null) tcPr = tc.addNewTcPr();
        CTTcBorders tcBorders = tcPr.getTcBorders(); if (tcBorders == null) tcBorders = tcPr.addNewTcBorders();
        if (border == Border.LEFT) {
            CTBorder left = tcBorders.getLeft(); if(left == null) left = tcBorders.addNewLeft();
            left.setVal(type);
        } else if (border == Border.TOP) {
            CTBorder top = tcBorders.getTop(); if(top == null) top = tcBorders.addNewTop();
            top.setVal(type);
        } else if (border == Border.BOTTOM) {
            CTBorder bottom = tcBorders.getBottom(); if(bottom == null) bottom = tcBorders.addNewBottom();
            bottom.setVal(type);
        } else if (border == Border.RIGHT) {
            CTBorder right = tcBorders.getRight(); if(right == null) right = tcBorders.addNewRight();
            right.setVal(type);
        }
    }

    /***********一种替代的方式 取出单元格边框 end********************/
}
